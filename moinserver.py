"""
A standalone server for moinmoin, intended as a stand-in while moinmoin
doesn't do python2.

To set this up on Debian bullseye (which still has python2), do:

sudo mkdir /var/legacy-moin
sudo chown www-data:www-data /var/legacy-moin
sudo -u www-data bash
cd /var/legacy-moin
virtualenv --system-site-packages -p python2.7 oldpython
. oldpython/bin/activate
pip install twisted
pip install moin

Then copy this file into legacy-moin.

A sysvinit start script for this could look like this:

===============================
#!/bin/sh
### BEGIN INIT INFO
# Provides:          oldmoin
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Legacy moinmoin
# Description:       A piece of twisted listening on port 10810
### END INIT INFO

INSTALL_ROOT=/var/legacy-moin
export VIRTUAL_ENV=$INSTALL_ROOT/oldpython
SCRIPT_LOCATION=$INSTALL_ROOT/moinserver.py
DAEMONOPTS="--pidfile /run/oldmoin.pid --oknodo --exec $INSTALL_ROOT/oldpython/bin/python2.7"

do_start() {
		start-stop-daemon --start $DAEMONOPTS \
			--make-pidfile --background --chuid www-data \
			-- $SCRIPT_LOCATION
}

do_stop() {
		start-stop-daemon --stop $DAEMONOPTS \
			--remove-pidfile \
			-- $SCRIPT_LOCATION
}


case "$1" in
	start)
		do_start
		;;
	stop)
		do_stop
		;;
	restart)
		do_stop
		do_start
		;;
	*)
		echo "USAGE: $0 start|stop|restart"
		;;
esac

exit 0
===============================

A systemd unit file should be correspondingly simpler.

This file is distributed under CC0  (a.k.a public domain).
"""

import os
import sys

from twisted.internet import endpoints
from twisted.web import resource
from twisted.web import server
from twisted.web.wsgi import WSGIResource
from twisted.internet import reactor

from MoinMoin.web.serving import make_application

sys.path[:0] = ["/etc/moin"]


CHILD = WSGIResource(
	reactor, 
	reactor.getThreadPool(), 
	make_application(shared=True))


class Root(resource.Resource):
	def getChild(self, name, request):
		return CHILD


site = server.Site(Root(), timeout=300)
endpoint = endpoints.TCP4ServerEndpoint(
	reactor, 10810, interface="localhost")
endpoint.listen(site)
reactor.run()
