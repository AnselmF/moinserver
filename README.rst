=============
moinserver.py
=============

This is a minimal web server for running moinmoin within a python2
virtual environment.  It is based on twisted, and I've found it to scale
well enough for reasonably busy sites.

The reason I've written this is that moinmoin_ has dropped out of Debian
bullseye, and most of its dependencies, too.  Meanwhile, there's still
no `python3-compatible moinmoin`_ in sight.  I can't keep the servers
I'm running moinmoin on on Debian buster forever, and I don't want some
completely unmanaged python2 installation where the system apache sees
it.

.. _python3-compatible moinmoin: https://moinmo.in/Python3
.. _moinmoin: https://moinmo.in/

This little thing allows one to run a moinmoin with relatively minimal
dependencies; I'm sure there are still security holes lurking in the
whole stack, but perhaps they won't blow up before moinmoin is updated.
My next step will still be to move this thing into a podmanised
container.

Meanwhile, here is how to use this, assuming Debian bullseye, which
still has python2 packaged as such, assuming you have the virtualenv and
python2.7 packages installed, starting from some convenient directory
writable for yourself::

  git clone https://codeberg.org/AnselmF/moinserver.git
  sudo mkdir /var/legacy-moin
  sudo cp moinserver/moinserver.py /var/legacy-moin/
  sudo chown www-data:www-data /var/legacy-moin
  sudo -u www-data bash
  cd /var/legacy-moin
  virtualenv --system-site-packages -p python2.7 oldpython
  . oldpython/bin/activate
  pip install twisted
  pip install moin

You can then run ``/var/legacy-moin/moinserver.py`` as a daemon.  As
sysvinit script might be::

  #!/bin/sh
  ### BEGIN INIT INFO
  # Provides:          oldmoin
  # Required-Start:    $local_fs $network
  # Required-Stop:     $local_fs
  # Default-Start:     2 3 4 5
  # Default-Stop:      0 1 6
  # Short-Description: Legacy moinmoin
  # Description:       A piece of twisted listening on port 10810
  ### END INIT INFO

  INSTALL_ROOT=/var/legacy-moin
  export VIRTUAL_ENV=$INSTALL_ROOT/oldpython
  SCRIPT_LOCATION=$INSTALL_ROOT/moinserver.py
  DAEMONOPTS="--pidfile /run/oldmoin.pid --oknodo --exec $INSTALL_ROOT/oldpython/bin/python2.7"

  do_start() {
      start-stop-daemon --start $DAEMONOPTS \
        --make-pidfile --background --chuid www-data \
        -- $SCRIPT_LOCATION
  }

  do_stop() {
      start-stop-daemon --stop $DAEMONOPTS \
        --remove-pidfile \
        -- $SCRIPT_LOCATION
  }


  case "$1" in
    start)
      do_start
      ;;
    stop)
      do_stop
      ;;
    restart)
      do_stop
      do_start
      ;;
    *)
      echo "USAGE: $0 start|stop|restart"
      ;;
  esac

  exit 0

A systemd unit ought to be trivial.

For every path that leads to a moinmoin (as configured in
``/etc/moin/farmconfig.py``), you will hten have a snippet like::

  location /my-wiki {
    proxy_set_header Host       $host;
    proxy_pass http://localhost:10810/my-wiki;
  }

  location ~ ^/moin_static[0-9]+/(.*) {
    alias /var/legacy-moin/oldpython/lib/python2.7/site-packages/MoinMoin/web/static/htdocs/$1;
  }

in nginx or::

  <Location /my-wiki>
    ProxyAddHeaders On
    ProxyPreserveHost On
  </Location>

  <Directory /var/legacy-moin/oldpython/lib/python2.7/site-packages/MoinMoin/web/>
    Require all granted
  </Directory>
  Alias /moin_static1911 /var/legacy-moin/oldpython/lib/python2.7/site-packages/MoinMoin/web/static/htdocs

  ProxyPassMatch  "(^/my-wiki.*)" http://localhost:10810$1

in apache (on which you may need to enable proxying manually, e.g., by
running ``a2enmod proxy; a2enmod proxy_http`` on Debian).
